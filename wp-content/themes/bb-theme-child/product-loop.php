<div class="product-grid swatch" itemscope itemtype="http://schema.org/ItemList">

    <div class="row product-row">
    <?php 

    $show_financing = get_option('sh_get_finance');

    $col_class = 'col-md-3 col-sm-4 col-xs-6';
    
    ?>
<?php 
$K = 1;
while ( have_posts() ): the_post(); 
      //collection field
      $collection = get_field('collection', $post->ID);
?>
    <div class="<?php echo $col_class; ?>">    
    <div class="fl-post-grid-post" itemprop="itemListElement" itemscope  itemtype="http://schema.org/ListItem">

    <meta itemprop="position" content="<?php echo $K;?>" />
        <?php // FLPostGridModule::schema_meta(); ?>
        <?php if(get_field('swatch_image_link')) { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                  <?php 
                  
                   $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
							
							
					?>
            <img class="list-pro-image" src="<?php  echo $image; ?>" alt="<?php the_title_attribute(); ?>" />
            
            <?php
            // exclusive icon condition
            if($collection == 'COREtec Colorwall' || $collection == 'Coretec Colorwall' ||  $collection == 'Floorte Magnificent') {    ?>
			<span class="exlusive-badge"><img src="<?php echo plugins_url( '/product-listing-templates/images/exclusive-icon.png', dirname(__FILE__) );?>" alt="<?php the_title(); ?>" /></span>
			<?php } ?>
                  
                </a>
            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>
            </div>

        <?php } ?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><i class="fa fa-caret-up" aria-hidden="true"></i></a>
        <div class="fl-post-grid-text product-grid btn-grey">
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"> <h3><?php the_field('collection'); ?></h3></a>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <h2 class="fl-post-grid-title" itemprop="name">
                  <?php echo get_field('color'); ?>
                </h2>
            </a>    
            <h4><?php the_field('brand'); ?></h4>
            <?php 

             
            
             $promsale =  json_decode(get_option('saleconfiginformation')); 

             //print_r($promsale)

             $sale_arr = array();

             $brand_arr = array();

             $i = 0 ;



             foreach ($promsale as $sale) {
                
                 if($sale->getCoupon == 1){

                    $brand_arr = array_merge($brand_arr,$sale->brandList);

                    $sale_end_date   =  date("d-m-Y", substr($sale->endDate, 0, 10)); 
                    $sale_start_date =  date("d-m-Y", substr($sale->startDate, 0, 10)); 

                    $sale_arr[$i]['promoCode'] = $sale->promoCode;
                    $sale_arr[$i]['name']      = $sale->name; 
                    $sale_arr[$i]['startDate'] = $sale_start_date; 
                    $sale_arr[$i]['endDate']   = $sale_end_date;   
                    $sale_arr[$i]['getCoupon'] = $sale->getCoupon;   
                    $sale_arr[$i]['brandList'] = $sale->brandList;   


                    $i++;
                 }
             }
           
            ?>
           <?php if( get_option('getcouponbtn') == 1){  ?>
                <a href="<?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplaceurl');}else{ echo '/flooring-coupon/'; } ?>" target="_self" class="fl-button getcoupon-btn" role="button" <?php //get_coupon_button_visibility($sale_arr,$brand_arr); ?> >
                <span class="fl-button-text"><?php if(get_option('getcouponreplace')==1){ echo get_option('getcouponreplacetext');}else{ echo 'GET COUPON'; }?></span>
            </a>
            </a><br />
            <?php } ?>
           

           
          
            <a class="link plp-home_estimate" href="/about-us/in-home-estimate/">In Home Estimate</a><br>
            <a class="link plp-req_financing" href="/about-us/financing/">Request Financing</a>
        </div>
    </div>
    </div>
<?php  $K++; endwhile; ?>
</div>
</div>