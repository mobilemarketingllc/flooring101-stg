<?php
/*
Plugin Name: WP ADA Compliance Check Basic
Description: Comply with SECTION 508 and WC3/WCAG Web Accessibility Standards. This easy to use plugin evaluates pages for the most common issues as they are published. Upgrade to the full version to unlock all the great features including complete scans of your website pages, posts, media library images and custom post types.
Version: 2.3.12
  Plugin URI: https://wordpress.org/plugins/wp-ada-compliance-check-basic/
  Author: AlumniOnline Web Services LLC
  Author URI: https://www.alumnionlineservices.com/php-scripts/wordpress-wp-ada-compliance-check/
  Text Domain: wp-ada-compliance-basic
*/
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;

/*********************************************
PLUGIN INSTALLATION
*******************************************/
register_activation_hook( __FILE__, 'wp_ada_compliance_basic_install' );
register_uninstall_hook( __FILE__, 'wp_ada_compliance_basic_uninstall');
/*********************************************
IMPORT RESOURCES
*******************************************/
// define update file path and basename
$wp_ada_compliance_basic_plugin_directory = __FILE__;
$wp_ada_compliance_basic_plugin_basename = plugin_basename( __FILE__ );

// add simple dom support (need to over ride max file size, if clashes with another install of simple dom there the max file size will be dependednt upon that installation)
if(!defined('MAX_FILE_SIZE')) define('MAX_FILE_SIZE', 6000000);
if(!class_exists ('simple_html_dom'))
include_once(plugin_dir_path( __FILE__ ).'res/simplehtmldom/simple_html_dom.php');

include(plugin_dir_path( __FILE__ ).'res/compliance_descriptions.php');
include(plugin_dir_path( __FILE__ ).'res/installation.php');
include(plugin_dir_path( __FILE__ ).'res/dashboard.php');
include(plugin_dir_path( __FILE__ ).'res/security.php');
include(plugin_dir_path( __FILE__ ).'res/settings.php'); 
include(plugin_dir_path( __FILE__ ).'res/errors.php'); 
include(plugin_dir_path( __FILE__ ).'res/purgedata.php');
foreach ($wp_ada_compliance_basic_def as $rows => $row){	
if(file_exists(plugin_dir_path( __FILE__ ).'res/rules/'.$rows.'.php'))
	include(plugin_dir_path( __FILE__ ).'res/rules/'.$rows.'.php'); 
}
include(plugin_dir_path( __FILE__ ).'res/content_validation.php'); 
include(plugin_dir_path( __FILE__ ).'res/reports.php'); 
include(plugin_dir_path( __FILE__ ).'res/sendmail.php');
include(plugin_dir_path( __FILE__ ).'res/content_posts.php'); 
include(plugin_dir_path( __FILE__ ).'res/elementor_editor.php'); 
include(plugin_dir_path( __FILE__ ).'res/beaverbuilder_editor.php'); 
include(plugin_dir_path( __FILE__ ).'res/block-editor.php');
include(plugin_dir_path( __FILE__ ).'res/vendor/persist-admin-notices-dismissal/persist-admin-notices-dismissal.php');

/*********************************************
FILTERS AND ACTIONS
*******************************************/
add_action( 'admin_enqueue_scripts', 'wp_ada_compliance_basic_admin_scripts' ); // import admin css file
add_action( 'wp_enqueue_scripts', 'wp_ada_compliance_basic_scripts' ); // import public css file
add_action( 'add_meta_boxes', 'wp_ada_compliance_basic_report_meta_box' ); // works in gutenburg and classic editor
add_action('admin_init', 'wp_ada_compliance_basic_admin_init'); // create admin settings
add_action( 'wp_loaded', 'wp_ada_compliance_basic_preprocessing' ); // monitor and process actions
add_filter( 'save_post', 'wp_ada_compliance_basic_validate_ada_post_compliance', 10, 2 ); // validate post content when saving
add_action('admin_menu', 'wp_ada_compliance_basic_admin_add_page'); // add admin page to menu
add_action( 'admin_menu', 'wp_ada_compliance_basic_admin_menu' ); // add admin submenu links to menu
add_action('admin_menu', 'wp_ada_compliance_basic_add_external_link_admin_submenu'); // add additional links to menu
add_action( 'update_option_wp_ada_compliance_basic_scan_rules', 'wp_ada_compliance_basic_update_scan_rule_ignore_options', 10, 2 ); // if scan rules change, update ignore settings
add_action('wp_dashboard_setup', 'wp_ada_compliance_basic_dashboard_widgets'); // add dashboard widget with stats
add_filter( "option_page_capability_wp_ada_compliance_basic_options", 'wp_ada_compliance_basic_set_role' ); // set capability for settings page
//add_action( 'delete_post', 'wp_ada_compliance_basic_remove_deleted_posts' );
add_action('plugins_loaded', 'wp_ada_compliance_basic_check_version'); // check version and update update database as required
add_action( 'admin_notices', 'wp_ada_compliance_basic_admin_notices' ); // display messages / admin notices

// add scan link to post and page list
add_filter('post_row_actions', 'wp_ada_compliance_basic_add_post_editor_link', 10, 2);
add_filter('page_row_actions', 'wp_ada_compliance_basic_add_post_editor_link', 10, 2);

// delete error when post is trashed 
add_filter( 'pre_delete_post', 'wp_ada_compliance_basic_delete_post', 10, 3 ); 

// add admin body class for jquery and iframe
add_filter( 'admin_body_class','wp_ada_compliance_basic_add_body_classes' );

// set wordpress option when importing content to disable scans during import
add_action('import_start', 'wp_ada_compliance_basic_start_import'); 
add_action('import_end', 'wp_ada_compliance_basic_end_import'); 

/************************************************
managet requests and start processes
************************************************/
function wp_ada_compliance_basic_preprocessing() {
    	// load think box and set sesssion where required
    if(is_admin() 
	   and (strstr($_SERVER['REQUEST_URI'],'admin.php?page=ada_compliance') or
		   strstr($_SERVER['REQUEST_URI'],'admin.php?page=wp-ada-compliance-basic-admin') or
	  		strstr($_SERVER['REQUEST_URI'],'/wp-admin/post.php') or 
	   		strstr($_SERVER['REQUEST_URI'],'/wp-admin/term.php'))){
           
                // ensure import monitoring is disabled
        wp_ada_compliance_basic_end_import();
        
            if ( !session_id() ) session_start();
	// include thick box
	add_thickbox();	
	}
    
	if(wp_ada_compliance_basic_check_is_admin() and strstr($_SERVER['REQUEST_URI'],'admin.php?page=ada_compliance')){
		
	// ignore error
	if ( isset( $_GET[ 'wpada_ignore' ] )) {
		wp_ada_compliance_basic_ignore_error( (int)$_GET['wpada_ignore'] );
	}
		
	// ignore rules
	elseif ( isset( $_GET['wpada_ignore_rule'] )) {
       // validate input
        wp_ada_compliance_basic_form_values();
		$_GET[ 'wpada_ignore_rule' ] = sanitize_text_field($_GET[ 'wpada_ignore_rule' ]);
		wp_ada_compliance_basic_ignore_scan_rule($_GET[ 'wpada_ignore_rule' ]);
			// remove records no longer being scanned
	 wp_ada_compliance_basic_purge_records();	
		$_SESSION['my_ada_important_notices'] = __('The selected rule is now being ignored.','wp-ada-compliance-basic');
	}	
    elseif(isset( $_GET[ 'refresh' ] ) ){
		// remove records no longer being scanned
	 wp_ada_compliance_basic_purge_records();	
	}
	// start scan of single post
	if ( isset($_GET[ 'scansingle' ]) and isset($_GET['postid']) and isset($_GET['type'])) {
	
	wp_ada_compliance_basic_start_single_scan();
	}
	// start scan of content
	if ( isset($_GET[ 'startscan' ]) and !isset($_GET['cpage'])) {
	wp_ada_compliance_basic_start_scan();
	}	
	}

}

/************************************************
manual or autoscan scan 
************************************************/
function wp_ada_compliance_basic_start_scan() {
		
		wp_ada_compliance_basic_purge_records();
			
		// scan post content in database
		wp_ada_compliance_basic_scan_ada_compliance_post();
			
	
}

/************************************************
rest single scan
************************************************/
function wp_ada_compliance_basic_rest_start_single_scan() {
	
	check_ajax_referer('wp_rest', '_wpnonce');
	
	if(isset($_GET['wpadarescan'])){

	 $values = explode('|',sanitize_text_field($_GET['wpadarescan'])); 
	 	
	$_GET['type'] = $values[0];	
	$_GET['postid'] = $values[1];	
			
	 wp_ada_compliance_basic_start_single_scan(); 
	
					// clear error message
	if(isset($_SESSION['my_ada_notices'])) $_SESSION['my_ada_notices'] = "";
	}
}

/************************************************
manual single scan
************************************************/
function wp_ada_compliance_basic_start_single_scan() {
	
       
		// enforce post type check
	$post_types = get_option('wp_ada_compliance_basic_posttypes',array('page','post','attachment'));	
    
	if(!in_array($_GET['type'],$post_types)) {
        return 0;
	}
	
		
	if(!isset($_GET['type']) or !isset($_GET['postid'])) return 0;
	$postid = (int)$_GET['postid'];
		
	wp_ada_compliance_basic_validate_ada_post_compliance($postid);	
	
	// clear error message if not scanning on save	
if(!isset($_GET['wpadarescan'])){		
if(isset($_GET['startscan']) or isset($_GET['scansingle']) or !array_key_exists('scantype',$postinfo)) 
	$_SESSION['my_ada_notices'] = "";
}
}

/******************************************
// include css and scripts
****************************************/
function wp_ada_compliance_basic_scripts() {	
	
wp_register_style( 'wp-ada-compliance-styles',  plugin_dir_url( __FILE__ ) .  'styles.css', array(), filemtime( plugin_dir_path( __FILE__ ) .  'styles.css' ) );	
wp_enqueue_style( 'wp-ada-compliance-styles' );		

    // font awesome  
//wp_register_style( 'wp-ada-compliance-font-awesome-styles', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css', array(), '' );	
//wp_enqueue_style( 'wp-ada-compliance-font-awesome-styles' );	 
		 
}

/******************************************
// add admin body class for jquery and iframe
****************************************/
function wp_ada_compliance_basic_add_body_classes( $classes ) {


if(is_admin() and strstr($_SERVER['REQUEST_URI'],'admin.php?page=ada_compliance/compliancereportbasic.php') and strstr($_SERVER['REQUEST_URI'],'iframe=1')){	
	$classes .= ' wpadaIframe ';
}
	
    return $classes;
     
}

/******************************************
// include css and scripts for admin features
****************************************/
function wp_ada_compliance_basic_admin_scripts() {
    
// start sessions to display messages
if(is_admin() and !strstr($_SERVER['REQUEST_URI'],'theme-editor.php') and !strstr($_SERVER['REQUEST_URI'],'plugin-editor.php') and !strstr($_SERVER['REQUEST_URI'],'site-health.php') 
   and !strstr($_SERVER['REQUEST_URI'],'admin-ajax.php')){    
	
wp_register_style( 'wp-ada-compliance-styles',  plugin_dir_url( __FILE__ ) .  'styles.css', array(), filemtime( plugin_dir_path( __FILE__ ) .  'styles.css' ) );	
wp_enqueue_style( 'wp-ada-compliance-styles' );		

    
    // font awesome  
wp_register_script( 'wp-ada-compliance-fontawesome-scripts',  '//kit.fontawesome.com/6afc8f9e49.js', array('jquery'), '', true);
wp_enqueue_script( 'wp-ada-compliance-fontawesome-scripts' );	     
	
/* tabs for admin page */
 wp_enqueue_script( 'jquery-ui-tabs' );		
	
// scripts to use jquery to ignore		
wp_register_script( 'wp-ada-compliance-basic-scripts',  plugin_dir_url( __FILE__ ) .  'res/scripts.php', array('jquery'), filemtime( plugin_dir_path( __FILE__ ) .  'res/scripts.php' ) );
wp_enqueue_script( 'wp-ada-compliance-basic-scripts' );	
	
wp_localize_script( 'wp-ada-compliance-basic-scripts', 'wpadacompliancebasicVariables',array(
	'showsummary' =>'<i class="fas fa-toggle-off"></i> '.__('Show Summary','wp-ada-compliance-basic'),
	'hidesummary' =>'<i class="fas fa-toggle-on"></i> '.__('Hide Summary','wp-ada-compliance-basic'),
	'working' => __( '<span class="adaworking">&nbsp;</span> Please wait while the report is being refreshed' ,'wp-ada-compliance-basic'),
   'inprogress' => '<span class="adaworking">&nbsp;</span> '.__( 'SCAN IN PROGRESS - Upgrade to the full version to enable deep scans which will scan your entire website. Deep scans will identify issues in theme files, shortcodes, widgets, archives and much more. The automatic scan feature will monitor your website for issues while you are offline and send detailed email reports. The full version has no limit on the number of pages or posts that may be scanned.','wp-ada-compliance-basic'),
	'refresh' => '<i class="far fa-check-circle" aria-hidden="true"></i> '.__( 'The report data has been refreshed.' ,'wp-ada-compliance-basic'),
'ignoreerror' => '<i class="fas fa-info-circle" aria-hidden="true"></i> '.__( 'The selected item is now being ignored.' ,'wp-ada-compliance-basic'),
    'ignorestatus' => '<i class="fas fa-info-circle" aria-hidden="true"></i> '.__( 'Ignore status has been saved.' ,'wp-ada-compliance-basic'),
    		'recheck' => '<i class="far fa-check-circle" aria-hidden="true"></i> '.__( 'Scan is complete and error results have been updated.' ,'wp-ada-compliance-basic'),
		'unignoreerror' => '<i class="fas fa-info-circle" aria-hidden="true"></i> '.__( 'The selected item has been removed from the ignore list.' ,'wp-ada-compliance-basic'),
	'ignoreerrorthis' => __( 'This Error' ,'wp-ada-compliance-basic'),
		'ignoreerrortitle' => '<i class="fas fa-info-circle" aria-hidden="true"></i> '.__( 'Ignore this instance of the error.' ,'wp-ada-compliance-basic'),
	'unignoreerrortitle' => '<i class="fas fa-info-circle" aria-hidden="true"></i> '.__( 'Remove ignore from this error instance.' ,'wp-ada-compliance-basic'),
	'ignorerule' => '<i class="fas fa-info-circle" aria-hidden="true"></i> '.__('The selected rule is now being ignored. You may re-enable this rule under plugin settings.','wp-ada-compliance-basic'),
	'ignoreruleconfirm' => __('By continuing this error will be removed from the results and all future scans. You may re-enable this scan rule under plugin settings.','wp-ada-compliance-basic'),
	 'resturl' => esc_url_raw(get_rest_url()),
	'nonce' => wp_create_nonce( 'wp_rest' ) 
	));	
    
}
}

/**********************************************
display notices
********************************************/
function wp_ada_compliance_basic_admin_notices(){
$allowed_html = array(
  'a' => array('href' => array(), 'target' => array(), 'class' => array()),
  'p' => array('class' => array()),
'span' => array('class' => array()),	
	'i' => array('class' => array(), 'aria-hidden' => array()),
  'strong' => array(),
  'br' => array(),
	'h2' => array(),
);	
	
	// clear on iframe display
if(isset($_GET['iframe']) or !strstr($_SERVER['REQUEST_URI'],'/wp-admin/post.php?post=')){
	$_SESSION['my_ada_notices'] = "";
}	
if(!empty($_SESSION['my_ada_notices']) and (stristr($_SERVER['PHP_SELF'], "post.php"))) {
	 echo '<div class="adaError"><h2>';
	_e('Web Accessibility Compliance Issues:','wp-ada-compliance-basic');
	echo '</h2>';
	echo '<p class="wp_ada_version_message">';
	_e('The full version will auto correct many issues and is packed full of time saving features. ','wp-ada-compliance-basic');
	echo '<a href="https://www.alumnionlineservices.com/php-scripts/wp-ada-compliance-check/">';
	_e('Learn more or upgrade to unlock time saving features. ','wp-ada-compliance-basic');
	echo'</a>';
	echo '</p>';
	echo wp_kses($_SESSION['my_ada_notices'], $allowed_html);
	echo '</div>';
  }
  elseif(!empty($_SESSION['my_ada_notices']) and stristr($_SERVER['PHP_SELF'], "admin.php")) {
	  echo '<div class="adaAllGood">';
	  echo wp_kses($_SESSION['my_ada_notices'], $allowed_html);
	  echo '</div>';
  }	
  if ( session_id() )	{
	unset ($_SESSION['my_ada_notices']);
  }
}

/*******************************************************
display html_validation notification
*********************************************************/
add_action( 'admin_notices', 'wp_ada_compliance_basic_html_validation_notification' );
function wp_ada_compliance_basic_html_validation_notification() {
    
if(!strstr($_SERVER['REQUEST_URI'],'admin.php?page=ada_compliance') and !strstr($_SERVER['REQUEST_URI'],'page=wp-ada-compliance-basic-admin') and !strstr($_SERVER['REQUEST_URI'],'/wp-admin/index.php')) return;
    
if (! PAnD::is_admin_notice_active( 'notice-htmlvalidation-30' ) ) {
		return;
	}	    

// html validation
if (!is_plugin_active('html-validation/html-validation.php') ) {
$notice = __('HTML code validation is an important part of ensuring ADA compliance of your website. Install the ', 'wp-ada-compliance-basic');
$notice .= '<a href="https://www.alumnionlineservices.com/php-scripts/html-validation/">';
$notice .= __('FREE HTML Validation plugin', 'wp-ada-compliance-basic');
$notice .= '</a>';
$notice .= __(' to find and correct HTML code issues.', 'wp-ada-compliance-basic');   

   
echo '<div data-dismissible="notice-htmlvalidation-30" class="notice notice-error is-dismissible wp-ada-compliance-notification" >';
echo $notice;
echo '</div>';
}
}

/*******************************************************
display facebook notification
*********************************************************/
add_action( 'admin_notices', 'wp_ada_compliance_basic_facebook_notification' );
function wp_ada_compliance_basic_facebook_notification() {
    
if(!strstr($_SERVER['REQUEST_URI'],'admin.php?page=ada_compliance') and !strstr($_SERVER['REQUEST_URI'],'page=wp-ada-compliance-basic-admin')) return;	
    
if (! PAnD::is_admin_notice_active( 'notice-facebook-30' ) ) {
		return;
	}	    
    
// add facebook like link
$wpa_ada_compliance_facebook_link = __('Follow us on Facebook to receive web accessibility tips in your news feed: ','wp-ada-compliance-basic').'<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v8.0&appId=301645814260711&autoLogAppEvents=1" nonce="DiVhmSyN"></script><div class="fb-like" data-href="https://www.facebook.com/WPADAPlugin" data-width="" data-layout="button" data-action="like" data-size="large" data-share="true"></div>';
   
echo '<div data-dismissible="notice-facebook-30" class="notice notice-warning is-dismissible wp-ada-facebook-like" >';
echo $wpa_ada_compliance_facebook_link;
echo '</div>';

}
/************************************************
add links to post/page list edit.php
***********************************************/
function wp_ada_compliance_basic_add_post_editor_link($actions, $post){
if ( !current_user_can( "edit_pages" ) ) return $actions;	
	
$post_types = get_option('wp_ada_compliance_basic_posttypes',array('page','post','attachment'));	
if(!in_array($post->post_type,$post_types)) return $actions;	
	
$title = __('Scan for Web Accessibility Issues','wp-ada-compliance-basic');	
$actions['scanpost'] = '<a href="'.esc_url(get_site_url()).'/wp-admin/admin.php?page=ada_compliance%2Fcompliancereportbasic.php&scansingle=1&postid=' . esc_attr($post->ID) . '&view=1&searchtitle=' . esc_attr($post->ID) . '&type=' . esc_attr($post->post_type) . '" title="'.esc_attr($title).'" >'.__('Check For Issues', 'wp-ada-compliance-basic').'</a>';
    
    $enablewave = get_option('wp_ada_compliance_basic_enablewave','true');
if($enablewave == 'true'){
$actions['evaluatewavepost'] = '<a href="http://wave.webaim.org/report#/'.esc_url(site_url()).'/?p='.esc_attr($post->ID).'">'.__('Evaluate with Wave','wp-ada-compliance-basic').'</a>';
    
// if html validation plugin not installed
if(!array_key_exists('htmlvalidate',$actions))    
$actions['w3cvalidatepost'] = '<a href="https://validator.w3.org/nu/?doc='.esc_url(site_url()).'/?p='.esc_attr($post->ID).'">'.__('Validate HTML','wp-ada-compliance-basic').'</a>';    
}
    
return $actions;
}

/******************************************
// remove startscan from paginate links
****************************************/
add_filter( 'paginate_links', 'wp_ada_compliance_basic_remove_query_args');
function wp_ada_compliance_basic_remove_query_args( $link ){
      
$link = filter_input( INPUT_GET, 'scansingle' )  ? remove_query_arg( 'scansingle', $link ): $link;
$link = filter_input( INPUT_GET, 'startscan' )  ? remove_query_arg( 'startscan', $link ): $link;      
    
    return $link;
}

/***********************************************************************************
// remove child nodes with simple dom
**********************************************************************************/
function wp_ada_compliance_basic_SimpleDOMRemoveChild(simple_html_dom_node $parentNode) {
$parentNode->innertext = '';
$error = $parentNode->save();
return $error;
}
/***********************************************************************************
// remove child nodes with php dom
**********************************************************************************/
function wp_ada_compliance_basic_DOMRemoveChild(DOMNode $parentNode) {

$newdoc = new DOMDocument();
libxml_use_internal_errors(true);
$newdoc->loadHTML("<ada></ada>");	
$cloned = $parentNode->cloneNode(TRUE);
	
 while ($cloned->hasChildNodes()) {
    $cloned->removeChild($cloned->firstChild);
  } 
$newdoc->appendChild($newdoc->importNode($cloned,TRUE));	
$error = preg_replace('/^<!DOCTYPE.+?>/', '', str_replace( array('<html>', '</html>', '<body>', '</body>','<ada>', '</ada>'), array('', '', '', '','',''), $newdoc->saveHTML()));
return $error;
}

/*************************************************************
get the inner html of a DOM element
*************************************************************/
function wp_ada_compliance_basic_GETinnerHTML(\DOMElement $element)
{
    $doc = $element->ownerDocument;

    $html = '';

    foreach ($element->childNodes as $node) {
        $html .= $doc->saveHTML($node);
    }

    return $html;
}

//check if is_admin or rest api for new editor
function wp_ada_compliance_basic_check_is_admin($ignore_is_admin = 0) {
if($ignore_is_admin == 0 and function_exists('is_admin') and is_admin()) return 1;	
  $prefix = rest_get_url_prefix( );
        if (defined('REST_REQUEST') && REST_REQUEST // (#1)
            || isset($_GET['rest_route']) // (#2)
                && strpos( trim( $_GET['rest_route'], '\\/' ), $prefix , 0 ) === 0)
            return true;

        // (#3)
        $rest_url = wp_parse_url( site_url( $prefix ) );
        $current_url = wp_parse_url( add_query_arg( array( ) ) );
        return strpos( $current_url['path'], $rest_url['path'], 0 ) === 0;
	
}

/***************************************
support various editors guttenberg, beaver builder, elementor
*****************************************/
// register endpoints to update button on editor screens
add_action( 'rest_api_init', function () {
  register_rest_route( 'wp_ada_compliance_basic/v1', '/errorstatus/(?P<id>\d+)', array(
    'methods' => 'GET',
    'callback' => 'wp_ada_compliance_basic_update_report_button',
	   'permission_callback' => function () {
			return current_user_can( 'edit_pages' );
		}
  ) );
	// dispaly error notice
register_rest_route( 'wp_ada_compliance_basic/v1', '/displaynotice/(?P<id>\d+)', array(
    'methods' => 'GET',
    'callback' => 'wp_ada_compliance_basic_admin_notices_editor',
	 'permission_callback' => function () {
			return current_user_can( 'edit_pages' );
		}
  ) );
} );



// update button
function wp_ada_compliance_basic_update_report_button($data) {  
	
check_ajax_referer('wp_rest', '_wpnonce');	
	
$post = get_post((int)$data['id']);

if(wp_ada_compliance_basic_reported_errors_check($post->ID, $post->post_type, 1)){
    $button =  '<a href="'.esc_url(get_site_url()).'/wp-admin/admin.php?page=ada_compliance/compliancereportbasic.php&view=1&errorid='.esc_attr($post->ID).'&type='.esc_attr($post->post_type).'&iframe=1&TB_iframe=true&width=900&height=550" class="thickbox btnwpada btnwpada-warning adareportlink">';
	$button .= __('Accessibility Report','wp-ada-compliance-basic');
	$button .= '</a>';
	}  
	else{
	 $button =  '<i class="fas fa-thumbs-up btnwpada" style="font-size: 200%; background-color: #005700; color: #fff; width: 100px; padding: 5px;" aria-hidden="true" title="'.__('No Issues','wp-ada-compliance-basic').'"></i>';	
	}
	
	return $button;
}

// display error notice
function wp_ada_compliance_basic_admin_notices_editor($data) {  
	
check_ajax_referer('wp_rest', '_wpnonce');
	
$post = get_post((int)$data['id']);
$notice = '';
if(wp_ada_compliance_basic_reported_errors_check($post->ID, $post->post_type, 1)){
   $notice .= __('This page has one or more web accessibility problems. View the ','wp-ada-compliance-basic');
	$notice .=  '<a href="'.esc_url(get_site_url()).'/wp-admin/admin.php?page=ada_compliance/compliancereportbasic.php&view=1&errorid='.esc_attr($post->ID).'&type='.esc_attr($post->post_type).'&iframe=1&TB_iframe=true&width=900&height=550" class="thickbox adareportlink">';
	$notice .= __('Accessibility Report','wp-ada-compliance-basic');
	$notice .= '</a>';
		$notice .= '<p class="wp_ada_version_message">';
	$notice .= __('The full version will auto correct many issues and is packed full of time saving features. ','wp-ada-compliance-basic');
	$notice .= '<a href="https://www.alumnionlineservices.com/php-scripts/wp-ada-compliance-check/">';
	$notice .= __('Learn more or upgrade to unlock time saving features. ','wp-ada-compliance-basic');
	$notice .='</a>';
	$notice .= '</p>';
	}  	
	return $notice;
}


?>