<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate absolute font tags
/********************************************************************/	
function wp_ada_compliance_basic_validate_absolute_fontsize($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
// get options
$changefontsize = 'false';	
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
$report_filtered_errors = 'true';	
	
// check if being scanned
if(in_array('absolute_fontsize', $wp_ada_compliance_basic_scanoptions)) return 1;
$fontsearchpatterns = array();
$fontsearchpatterns[] = "|font\-size:\s?([\d]+)pt|i";
$fontsearchpatterns[] = "|font\-size:\s?([\d]+)px|i";
	$fontsearchpatterns[] = "|font:\s?[\w\s\d*\s]*([\d]+)pt|i";
$fontsearchpatterns[] = "|font:\s?[\w\s\d*\s]*([\d]+)px|i";

foreach($fontsearchpatterns as $pattern){

	if(preg_match_all($pattern, $content, $matches,PREG_PATTERN_ORDER)){
	$matchsize = sizeof($matches);
	
	for($i=0; $i < $matchsize; $i++){
        if(isset($matches[0][$i]) and $matches[0][$i] != ""){
		
			
		$absolute_fontsize_errorcode = htmlspecialchars($matches[0][$i]); 
		

		// save error
		if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"absolute_fontsize",$absolute_fontsize_errorcode)){
			
		$insertid = wp_ada_compliance_basic_insert_error($postinfo, "absolute_fontsize", $wp_ada_compliance_basic_def['absolute_fontsize']['StoredError'], $absolute_fontsize_errorcode);
		}
		
	}
	}
	}
	
}
	return 1;
} 
?>