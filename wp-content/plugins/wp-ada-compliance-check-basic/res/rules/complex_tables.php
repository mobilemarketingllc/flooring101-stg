<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate complex tables	
/********************************************************************/	
function wp_ada_compliance_basic_validate_complex_tables($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('complex_tables', $wp_ada_compliance_basic_scanoptions)) return 1;	

$tables = $dom->find('table');

foreach ($tables as $table) {	

$tablecode = $table->outertext;

if((stristr($table->getAttribute('role'), "presentation"))
  and stristr($tablecode, 'rowspan')){

	
	// save error
	if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"complex_tables", $tablecode)){		
	$insertid = wp_ada_compliance_basic_insert_error($postinfo,"complex_tables", $wp_ada_compliance_basic_def['complex_tables']['StoredError'],  $tablecode);	
	}

}
}
return 1;
}

?>