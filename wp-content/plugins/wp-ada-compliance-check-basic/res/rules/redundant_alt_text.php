<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate redundant alt tags
/********************************************************************/
function wp_ada_compliance_basic_validate_redundant_alt_text($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = new DOMDocument;
libxml_use_internal_errors(true);
$dom->loadHTML($content);	

// get options
$strip_redundant_alt_txt = 'false';	
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
$report_filtered_errors = 'true';	
	
// check if being scanned
if(in_array('redundant_alt_text', $wp_ada_compliance_basic_scanoptions)) return 1;

/********************************************************************/	
// validate redundant alt attributes on images
/********************************************************************/	
	
$images = $dom->getElementsByTagName('img');
foreach ($images as $image) {
		if($image->getAttribute('alt') != "")	{
		if(isset($captioncode)) unset($captioncode);
			$pattern  = '/'."\[caption.*\](.*?)alt=[\"\']\b".preg_quote(strtolower(trim($image->getAttribute('alt'))),'/')."\b[\"\'](.*?)\b".preg_quote(strtolower(trim($image->getAttribute('alt'))),'/')."\b\[\/caption\]".'/';	
			
		if(preg_match($pattern,$content,$captioncode)){
			if(!stristr($captioncode[0],'<a')){ // if not inside an anchor
		
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"redundant_alt_text", $caption_code))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"redundant_alt_text", $wp_ada_compliance_basic_def['redundant_alt_text']['StoredError'], $caption_code);
			

			
			}
			}
		}

}// end caption validate
	
/********************************************************************/
	// check figure captions
/********************************************************************/	
$anchor = "";	
$figures = $dom->getElementsByTagName('figure');
foreach ($figures as $figure) {
		
		$figurecaption = $figure->getElementsByTagName('figcaption')->item(0);
		$image = $figure->getElementsByTagName('img')->item(0);
		$anchor = $figure->getElementsByTagName('a')->item(0);
			if(isset($figurecaption)){
			$figcaptioncode = $figurecaption->textContent; 
		
			if($anchor == "" and isset($image) and $figcaptioncode == strtolower(trim($image->getAttribute('alt'))) and strtolower(trim($image->getAttribute('alt'))) != ""){
				
				
				$caption_code = $dom->saveXML($figure, LIBXML_NOEMPTYTAG);
				// save error
				if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"redundant_alt_text", $caption_code))
				$insertid = wp_ada_compliance_basic_insert_error($postinfo,"redundant_alt_text", $wp_ada_compliance_basic_def['redundant_alt_text']['StoredError'], $caption_code);


			
		}
	}
}// end figure caption validate	
	
/********************************************************************/	
// check alt text on image inside captions marked up with a div
/********************************************************************/	
$figuredivs = $dom->getElementsByTagName('div');
foreach ($figuredivs as $figure) {

	if(stristr($figure->getAttribute('class'),'wp-caption')){	
			
		$figurecaption = $figure->getElementsByTagName('p')->item(0);
		$anchor = $figure->getElementsByTagName('a')->item(0);
		if($anchor == "" and isset($figurecaption) and stristr($figurecaption->getAttribute('class'),'wp-caption-text')){	
			$image = $figure->getElementsByTagName('img')->item(0);
			$figcaptioncode = $figurecaption->textContent; 

			if(isset($image) 
			   and strtolower(trim($figcaptioncode)) == strtolower(trim($image->getAttribute('alt'))) 
			   and $image->getAttribute('alt') != ""){
				
				$caption_code = $dom->saveXML($figure, LIBXML_NOEMPTYTAG);
				// save error
				if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"redundant_alt_text", $caption_code))
				$insertid = wp_ada_compliance_basic_insert_error($postinfo,"redundant_alt_text", $wp_ada_compliance_basic_def['redundant_alt_text']['StoredError'], $caption_code);


				
	}
}	
	}
}


/********************************************************************/	
// redundant alt text on image inside anchor
/********************************************************************/	
	
$links = $dom->getElementsByTagName('a');
foreach ($links as $link) {
		$images = $link->getElementsByTagName('img');
		foreach ($images as $image) {
		if($image->getAttribute('alt') != ""){
			
      if (isset($link) 
		   and isset($image) 
		   and (strtolower(trim($link->nodeValue)) == strtolower(trim($image->getAttribute('alt')))
			or strtolower(trim($image->getAttribute('title'))) == strtolower(trim($image->getAttribute('alt')))
			   ) 
		   
		  ) {
			$redeidantalttag = $dom->saveXML($link, LIBXML_NOEMPTYTAG);
		
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"redundant_alt_text", $redeidantalttag))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"redundant_alt_text", $wp_ada_compliance_basic_def['redundant_alt_text']['StoredError'], $redeidantalttag);
			
			
	   		}
		}
		}
}

/********************************************************************/	
// redundant alt text on image not inside an anchor
/********************************************************************/	
		$images = $dom->getElementsByTagName('img');
		foreach ($images as $image) {
		if($image->getAttribute('alt') != "" and $image->getAttribute('title') !=""){
			
       if (isset($image) 
		   and wp_ada_compliance_basic_compare_strings($image->getAttribute('title'),$image->getAttribute('alt'))
		  ) {
			$redeidantalttag = $dom->saveXML($image, LIBXML_NOEMPTYTAG);
		
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"redundant_alt_text", $redeidantalttag))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"redundant_alt_text", $wp_ada_compliance_def['redundant_alt_text']['StoredError'], $redeidantalttag);
			
			
	   		}
}
		}	
	
return 1;
} 
?>