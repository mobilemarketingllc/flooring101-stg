<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/****************************************
media library image with invalid alt text
****************************************/
function wp_ada_compliance_basic_validate_img_alt_invalid_media($content, $postinfo){
	
global $wp_ada_compliance_basic_def;

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('img_alt_invalid_media', $wp_ada_compliance_basic_scanoptions)) return 1;
	
	$alt_text = get_post_meta($postinfo['postid'], '_wp_attachment_image_alt', true);
	$imagecode = '<img src="'.esc_url(site_url()).'/wp-content/uploads/'.strip_tags($content).'" width="150" height="75" alt="'.$alt_text.'" />';
	
// check for program space holders
	for($i=1; $i < 10; $i++) {
			if(strtolower($alt_text) == __('picture '.$i,'wp-ada-compliance-basic')) $error = 1;
			if(strtolower($alt_text) == __('image '.$i,'wp-ada-compliance-basic')) $error = 1;
			if(strtolower($alt_text) == __('spacer '.$i,'wp-ada-compliance-basic')) $error = 1;
			if(strtolower($alt_text) == __('000'.$i,'wp-ada-compliance-basic')) $error = 1;
			if(strtolower($alt_text) == __('intro#'.$i,'wp-ada-compliance-basic')) $error = 1;
			//	if(strtolower($alt_text) == __('sinclair'.$i,'wp-ada-compliance-basic')) $error = 1;
		//if(strtolower($alt_text) == __('filtereasypost'.$i,'wp-ada-compliance-basic')) $error = 1;
		}
	
	if(isset($error) 
	   or trim($alt_text) == '*'
	   or stristr($alt_text,__('alt""','wp-ada-compliance-basic'))
	   or stristr($alt_text,__('alt=""','wp-ada-compliance-basic'))
	   or stristr($alt_text,__('image of','wp-ada-compliance-basic'))
	   or stristr($alt_text,__('graphic of','wp-ada-compliance-basic'))
	   or stristr($alt_text,__('photo of','wp-ada-compliance-basic'))
	   or stristr($alt_text,__('Featured Image','wp-ada-compliance-basic'))
	   or strstr($alt_text,__('IMG ','wp-ada-compliance-basic'))
	   or strtolower($alt_text) == __('picture','wp-ada-compliance-basic')
	   or strtolower($alt_text) == __('image','wp-ada-compliance-basic')
	    or strtolower($alt_text) == __('logo','wp-ada-compliance-basic')
	   or stristr($alt_text,__('image001','wp-ada-compliance-basic'))
	   	or strtolower($alt_text) == __('spacer','wp-ada-compliance-basic')) {
		if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"img_alt_invalid_media", $imagecode))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"img_alt_invalid_media", $wp_ada_compliance_basic_def['img_alt_invalid_media']['StoredError'], $imagecode);
			

			// display error
			if(!wp_ada_compliance_basic_ignore_check("img_alt_invalid_media", $postinfo['postid'], $imagecode, $postinfo['type'])) {
			$_SESSION['my_ada_notices'] .= '<p>';
			$_SESSION['my_ada_notices'] .= $wp_ada_compliance_basic_def['img_alt_invalid_media']['DisplayError'];
			if($wp_ada_compliance_basic_def['img_alt_invalid_media']['Reference'] != "") $_SESSION['my_ada_notices'] .= ' <a href="'.$wp_ada_compliance_basic_def['img_alt_invalid_media']['ReferenceURL'].'" target="_blank" class="adaNewWindowInfo">'.$wp_ada_compliance_basic_def['img_alt_invalid_media']['Reference'].' <i class="fa fa-external-link" aria-hidden="true"><span class="wp_ada_hidden">'.__('opens in a new window', 'wp-ada-compliance-basic').'</span></i></a>';  
				$_SESSION['my_ada_notices'] .= $postinfo['ada_complianceview_error_link'];
			$_SESSION['my_ada_notices'] .= '</p>';
			
			}
	}
	}
?>