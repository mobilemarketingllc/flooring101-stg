<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate page structure, look for headings that are not in order
/********************************************************************/	
function wp_ada_compliance_basic_validate_incorrect_heading_order($content, $postinfo){

global $wp_ada_compliance_basic_def;

$dom = str_get_html($content);		

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
$wp_ada_compliance_basic_starting_H_level = get_option('wp_ada_compliance_basic_starting_H_level','h2');	
	
// check if being scanned
if(in_array('incorrect_heading_order', $wp_ada_compliance_basic_scanoptions)) return 1;	


$errorcode = '';
$h1 = count($dom->find('h1'));	
$h2 = count($dom->find('h2'));
$h3 = count($dom->find('h3'));
$h4 = count($dom->find('h4'));
$h5 = count($dom->find('h5'));
$h6 = count($dom->find('h6'));	

if($h2 and !$h1 and $wp_ada_compliance_basic_starting_H_level == 'h1') {
$errorcode .= __('h2 without h1; ', 'wp-ada-compliance-basic');
$h2s = $dom->find('h2');
foreach ($h2s as $h2value){
 $errorcode .= $h2value->outertext;
}	
}	
if($h3 and !$h2) {
$errorcode .= __('h3 without h2; ', 'wp-ada-compliance-basic');
$h3s = $dom->find('h3');
foreach ($h3s as $h3value){
 $errorcode .= $h3value->outertext;
}	
}
if($h4 and !$h3) {
$errorcode .= __('h4 without h3; ', 'wp-ada-compliance-basic');
$h4s = $dom->find('h4');
foreach ($h4s as $h4value){
 $errorcode .= $h4value->outertext;
}	
}
if($h5 and !$h4) {
$errorcode .= __('h5 without h4; ', 'wp-ada-compliance-basic');
$h5s = $dom->find('h5');
foreach ($h5s as $h5value){
 $errorcode .= $h5value->outertext;
}		
}
if($h6 and !$h5) {
$errorcode .= __('h6 without h5; ', 'wp-ada-compliance-basic');
$h6s = $dom->find('h6');
foreach ($h6s as $h6value){
 $errorcode .= $h6value->outertext;
}		
}

if($errorcode != ''){
$errorcode = 'Issues: '.$errorcode;	
	
// save error
if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"incorrect_heading_order", $errorcode))
$insertid = wp_ada_compliance_basic_insert_error($postinfo,"incorrect_heading_order", $wp_ada_compliance_basic_def['incorrect_heading_order']['StoredError'], $errorcode);


}	
return 1;
}
?>