<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate incorrect language markup
/********************************************************************/	
function wp_ada_compliance_basic_validate_missing_lang_attr_part($content, $postinfo){
		
global $wp_ada_compliance_basic_def;
	
// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('missing_lang_attr_part', $wp_ada_compliance_basic_scanoptions)) return 1;
	
// check if page content is wrapped in a div
//if(stristr($content,'<div lang=')) return 1;	
	
if(!function_exists('mb_convert_encoding')) return 1;
else // correct issues with encoding when website is using non utf-8
$content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");	
	
$dom = str_get_html($content);	
		

$html = $dom->find('html');
$pagelangdefault = get_option('wp_ada_compliance_basic_language_code','en');
	
$langparts = explode('-',$pagelangdefault);
if(is_array($langparts) and $langparts[0] !='') $pagelangdefault = $langparts[0];

// tags to check
$tags = array('p','h1','h2','h3','h4','h5','h6');	
	
foreach ($tags as $tag) {		
	
foreach ($dom->find($tag) as $element) {
if(!strstr($element->innertext,'lang=')) {   
	$pagelang = '';
if($element->getAttribute('lang') != '') $pagelang = $element->getAttribute('lang');
elseif($element->parent()->getAttribute('lang') != '') $pagelang = $element->parent()->getAttribute('lang');
elseif($element->getAttribute('xml:lang') != '') $pagelang = $element->getAttribute('xml:lang'); 
elseif($element->parent()->getAttribute('xml:lang') != '') $pagelang = $element->parent()->getAttribute('xml:lang');    
    
    
if(!isset($pagelang) or $pagelang == '') $pagelang = $pagelangdefault;  
    
$html = $element->plaintext;	

if(strip_tags($html) != ""){		
$lang = wp_ada_compliance_basic_getTextLanguage($html, 'en');
if($lang != $pagelang){
			
		
			$errorcode = '('.__('Language Detected: ','wp-ada-compliance-basic').$lang.') '.$html;	
	
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"missing_lang_attr_part", $errorcode)){
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"missing_lang_attr_part", $wp_ada_compliance_basic_def['missing_lang_attr_part']['StoredError'],  $errorcode);
			}
			
			
	}
}
}
}
}
	return 1;
} 

/******************************************************
check common non-english words for language
********************************************************/
function wp_ada_compliance_basic_getTextLanguage($text, $default) {
	$text = strip_tags($text);
      $supported_languages = array('en','de','es','fr','la');
// french word list  // http://french.languagedaily.com/wordsandphrases/most-common-words
  $wordList['fr'] = array ('être', 'avoir', 'je', 'de', 'ne', 'pas', 'le', 'la', 'tu', 'vous', 'il', 'et', 'à', 'un', 'qui', 'aller', 'les', 'en', 'ca', 'faire', 'tout', 'on', 'que', 'ce', 'une', 'mes', 'pour', 'se', 'des', 'dire', 'pouvoir', 'vouloir', 'mais', 'où', 'savoir', 
'elle', 'dans', 'mais', 'nous', 'me', 'du', 'elle', 'avec', 'moi', 'si', 'ici', 'sur', 'cette', 'devoir','bon', 'rien', 'lui', 'croire', 'très', 'attendre', 'ces', 'même');
     
	 // spanish word list // https://en.wikipedia.org/wiki/Most_common_words_in_Spanish
 $wordList['es'] = 
 array ('de', 'la', 'que', 'el', 'en', 'los', 'se', 'del', 'las', 'un', 'por', 'con', 'no', 'una', 'su', 'para', 'es', 'al', 'más', 'pero', 'sus', 'le', 'ha', 'me', 'si', 'sobre', 'sin', 'este', 'ya', 'entre', 'cuando', 'todo', 'esta','ser','dos','también', 'fue', 'había', 'muy', 'años', 'hasta','desde', 'está', 'mi', 'porque', 'qué', 'sólo', 'han', 'vez', 'puede', 'todos','así','nos','parte','tiene','él','donde', 'tu', 'yo', 'ese','españa', 'navidad', 'estaba', 'verdad', 'tiempo', 'señor', 'esa', 'casa', 'quieres', 'dónde', 'nunca', 'quién', 'hola', 'sabes', 'voy');
  
// German word list // from http://wortschatz.uni-leipzig.de/Papers/top100de.txt
$wordList['de'] = array ('der', 'die', 'und', 'in', 'den', 'von', 'zu', 'das', 'mit', 'sich', 'auf', 'für', 'ist', 'im', 'bei', 'gut', 'können',
	  'dem', 'nicht', 'ein', 'du', 'eine','ich','es','sie','der','wir','er','mir','ja','wie', 'den', 'mich', 'hier', 'wenn', 'einen','uns','dann');
  // English word list
  // from http://en.wikipedia.org/wiki/Most_common_words_in_English
$wordList['en'] = array ('the', 'be', 'to', 'of', 'and', 'in', 'that', 'have', 'it', 'for', 'not', 'on', 'with', 'he', 
	  'as', 'you', 'do', 'at', 'no', 'me', 'ha', 'sin', 'entre', 'this', 'but', 'his','by','from','they','we','say','her','she','or','an','will', 'my','one','all', 'would', 'there', 'their','what', 'so', 'up', 'out', 'if','about', 'who','get','which','go', 'me', 'when','make','can','like', 'time','no', 'just','him','know','take','people','into','year' ,'your','good','some','could','them', 'see', 'other', 'than');
	
$wordList['la'] = array ('quod', 'ut', 'autem', 'et', 'apud', 'habet', 'ego', 'iit', 'quia', 'non', 'vos', 'facite', 'nihil', 'mihi', 'haec', 'autem', 'eius','ex','nobis','dicens:','uel','tantum','sed','eiusmod','incididunt','accipere','populus','excepteur','velit','bonum','aliquid','poterat','illis', 'videre', 'alium', 'quam','dolore','tempor','pariatur','officia','nisi','ut','magna','tempore', 'intellegi', 'convenire','lorem','ipsum','adipisicing','consectetur','sit','amet','elit','iusto','odio','dignissimos','ducimus','vero');
    
      // clean out the input string - note we don't have any non-ASCII 
      // characters in the word lists... change this if it is not the 
      // case in your language wordlists!
       $text = preg_replace("/[^[]_A-Za-z]/", ' ', $text);
     
	// count the occurrences of the most frequent words
      foreach ($supported_languages as $language) {
        $counter[$language]=0;
      }
      for ($i = 0; $i < 76; $i++) {
        foreach ($supported_languages as $language) {
		if(array_key_exists($i,$wordList[$language]))
          $counter[$language] = $counter[$language] + substr_count(strtolower($text), ' ' .$wordList[$language][$i] . ' ');
        }
      }

      // get max counter value
      $max = max($counter);
      $maxs = array_keys($counter, $max);
      // if there are two winners - fall back to default!
      if (count($maxs) == 1) {
        $winner = $maxs[0];
        $second = 0;
        // get runner-up (second place)
        foreach ($supported_languages as $language) {
          if ($language <> $winner) {
            if ($counter[$language]>$second) {
              $second = $counter[$language];
            }
          }
        }
        // apply arbitrary threshold of 10
		// $percent = ($second / $max);
		
      // if ($percent > 0.1) {
		   
		   return $winner;
     //  } 
      }
	
      return $default;
    }
?>