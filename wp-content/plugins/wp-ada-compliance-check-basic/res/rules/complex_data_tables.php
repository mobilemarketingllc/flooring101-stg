<?php 
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate complex data tables	
/********************************************************************/	
function wp_ada_compliance_basic_validate_complex_data_tables($content, $postinfo){
	
global $wp_ada_compliance_basic_def;

// check if being scanned
    $wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
if(in_array('complex_data_tables', $wp_ada_compliance_basic_scanoptions)) return 1;
    
$dom = str_get_html($content);
$tables = $dom->find('table');

foreach ($tables as $table) {	
$tablecode = $table->outertext;
$headerrows = 0;   
$rows = $table->find('tr');
    
foreach ($rows as $row) {
 if(is_object($row->children(1)) and $row->children(1)->tag == 'th' and $row->children(1)->getAttribute('scope') == 'col' and !$row->children(1)->hasAttribute('headers')) {
       $headerrows++;  
      
   }
}

if($headerrows > 1){
	
	// save error
	if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"complex_data_tables", $tablecode)){		
	$insertid = wp_ada_compliance_basic_insert_error($postinfo,"complex_data_tables", $wp_ada_compliance_basic_def['complex_data_tables']['StoredError'],  $tablecode);	
	}

}
}
return 1;
}

?>