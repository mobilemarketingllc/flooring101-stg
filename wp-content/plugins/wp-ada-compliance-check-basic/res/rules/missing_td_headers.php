<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// look for headers referencing missing ids	
/********************************************************************/	
function wp_ada_compliance_basic_validate_missing_td_headers($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
		
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('missing_td_headers', $wp_ada_compliance_basic_scanoptions)) return 1;	

$tables = $dom->find('table');

foreach ($tables as $table) {	

$tablecode = $table->outertext;

if($table != "" and stristr($tablecode, "<th")){	


	$tablecells = $table->find('td');
	$tableheadercells = $table->find('th');
	
		    // look for headers referencing missing ids
	$headerids = array();
	foreach ($tablecells as $td) {			
		$headerids1 = explode(" ",$td->getAttribute('headers'));
		$headerids = array_merge($headerids, $headerids1);
	}
	foreach ($tableheadercells as $th) {
		$headerids2 = explode(" ",$th->getAttribute('headers'));
		$headerids = array_merge($headerids, $headerids2);
	}
		
	// look for headers referencing missing ids
	$additionalerrorInfo =  __("Missing Headers: ","wp-ada-compliance-basic");
	foreach ($tableheadercells as $th) {
	if(!in_array($th->getAttribute('id'),$headerids)) 
		$additionalerrorInfo .= $th->getAttribute('id')." ";
	}
			
			if($additionalerrorInfo !=  __("Missing Headers: ","wp-ada-compliance-basic")){
			$additionalerrorInfo .= $tablecode;	  
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"missing_td_headers", $additionalerrorInfo)){		
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"missing_td_headers", $wp_ada_compliance_basic_def['missing_td_headers']['StoredError'],  $additionalerrorInfo);	
			}
				
		}
	
	}

}
return 1;
}

?>