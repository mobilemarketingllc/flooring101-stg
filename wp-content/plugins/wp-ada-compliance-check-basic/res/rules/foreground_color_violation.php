<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// look for foreground colors without background colors or vice versa
/********************************************************************/	
function wp_ada_compliance_basic_validate_foreground_color_violation($content, $postinfo){
global $wp_ada_compliance_basic_def;
	
// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('foreground_color_violation', $wp_ada_compliance_basic_scanoptions)) return 1;

if($postinfo['type'] != 'css')	{

$dom = str_get_html($content);	
	
$elements = $dom->find('*');

foreach ($elements as $element) {			
	
if(isset($element) 
   and (stristr($element->getAttribute('style'), 'color:') 
	or stristr($element->getAttribute('style'), 'background:')
	   or stristr($element->getAttribute('style'), 'background-color:'))){

	$foreground = "";
	$background = "";

	// get background color
	preg_match('/background-color:\s*(#(?:[0-9a-f]{2}){2,4}|#[0-9a-f]{3}|(?:rgba?|hsla?)\((?:\d+%?(?:deg|rad|grad|turn)?(?:,|\s)+){2,3}[\s\/]*[\d\.]+%?\)\s*(!important)*)/i',$element->getAttribute('style'), $matches, PREG_OFFSET_CAPTURE);
	if(isset($matches[1][0]) and $matches[1][0] != "") $background = $matches[1][0];
	else {
		preg_match('/background:\s*(rgb\(\s*\d{1,3},\s*\d{1,3},\s*\d{1,3}\)|\#*[\w]{3,25}\s*(!important)*)/i',$element->getAttribute('style'), $matches, PREG_OFFSET_CAPTURE);
		if(isset($matches[1][0]) and $matches[1][0] != "")	
			$background = wp_ada_compliance_basic_check_color_match($matches[1][0]);
	}
	
		
	// get foreground color
	preg_match('/[\s|\"|\']*[^-]color:\s*(#(?:[0-9a-f]{2}){2,4}|#[0-9a-f]{3}|(?:rgba?|hsla?)\((?:\d+%?(?:deg|rad|grad|turn)?(?:,|\s)+){2,3}[\s\/]*[\d\.]+%?\)\s*(!important)*)/i',$element->getAttribute('style'), $matches, PREG_OFFSET_CAPTURE);
	if(isset($matches[1][0]) and $matches[1][0] != "") $foreground = $matches[1][0];

	if(($foreground == "" and $background != "") or ($foreground != "" and $background == "")){

// save error
	$foreground_color_violation_errorcode = wp_ada_compliance_basic_SimpleDOMRemoveChild($element);
	
		if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"foreground_color_violation", $foreground_color_violation_errorcode)){
			
		$insertid = wp_ada_compliance_basic_insert_error($postinfo, "foreground_color_violation", $wp_ada_compliance_basic_def['foreground_color_violation']['StoredError'], $foreground_color_violation_errorcode);
		}
		
		
	}
}
}
}
// // parse and scan style tag content in post files
if($postinfo['type'] != 'css' and stristr($content, '<style'))	{
// check links in content for style tags
$dom = str_get_html($content);

foreach ($dom->find('style') as $style) {	

	$css_array = wp_ada_compliance_basic_parce_style_content($style->innertext);
	
	wp_ada_compliance_basic_scan_css_content_for_foreground_violation($css_array, $postinfo);
}
}
// parse and scan css file content
if($postinfo['type'] == 'css')	{
$css_array = wp_ada_compliance_basic_parce_style_content($content);	

wp_ada_compliance_basic_scan_css_content_for_foreground_violation($css_array, $postinfo);

} 
}
	

/****************************************************************
parse css file to make it easy to search
*****************************************************************/
function wp_ada_compliance_basic_parce_style_content($css){
$css = preg_replace("%/\*(?:(?!\*/).)*\*/%s", " ",$css);	
$css_array = array(); // master array to hold all values
$element = explode('}', $css);
foreach ($element as $element) {
    // get the name of the CSS element
    $a_name = explode('{', $element);
    $name = $a_name[0];
    // get all the key:value pair styles
    $a_styles = explode(';', $element);
    // remove element name from first property element
    $a_styles[0] = str_replace($name . '{', '', $a_styles[0]);
    // loop through each style and split apart the key from the value
    $count = count($a_styles);
	$counter = 0;
    for ($a=0;$a<$count;$a++) {
        if ($a_styles[$a] != '') {
					$a_styles[$a] = str_ireplace('https://', '//', $a_styles[$a]);
			$a_styles[$a] = str_ireplace('http://', '//', $a_styles[$a]);	
            $a_key_value = explode(':', $a_styles[$a]);
            // build the master css array
			if(array_key_exists(1, $a_key_value))
            $css_array[trim($counter.$name)][trim(strtolower($a_key_value[0]))] = trim($a_key_value[1]);
        }
		$counter++;
    }               
}
	return $css_array;
}
	
/*********************************************************
scan the content from a css file or style tag inside a post
*********************************************************/
function wp_ada_compliance_basic_scan_css_content_for_foreground_violation($css_array, $postinfo){
global $wp_ada_compliance_basic_def;	
foreach($css_array as $element => $rules){

if(array_key_exists('background-color', $rules) or array_key_exists('color', $rules) or array_key_exists('background', $rules))  {
	
if(array_key_exists('color', $rules))	$foreground = $rules['color'];
else $foreground = "";
	
if(array_key_exists('background-color', $rules)) $background = $rules['background-color'];
elseif(array_key_exists('background', $rules)) $background = wp_ada_compliance_basic_check_color_match($rules['background']);
else $background = "";
	
	
if(($foreground == "" and $background != "") or ($foreground != "" and $background == "")){
			
		
	
				$foreground_color_violation_errorcode = $element.'{';
					foreach($rules as $key => $value){
						$foreground_color_violation_errorcode .= $key.': '.$value.'; ';
					}
				$foreground_color_violation_errorcode .= '}';

		// save error
		if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"foreground_color_violation", $foreground_color_violation_errorcode)){
			
		$insertid = wp_ada_compliance_basic_insert_error($postinfo, "foreground_color_violation", $wp_ada_compliance_basic_def['foreground_color_violation']['StoredError'], $foreground_color_violation_errorcode);
		}
		
		
}
}
}
}
/*********************************************************
check background style for color
*********************************************************/
function wp_ada_compliance_basic_check_color_match($background_rule){	
	
// standard 147 HTML color names
    $colors  =  array(
        'aliceblue'=>'F0F8FF',
        'antiquewhite'=>'FAEBD7',
        'aqua'=>'00FFFF',
        'aquamarine'=>'7FFFD4',
        'azure'=>'F0FFFF',
        'beige'=>'F5F5DC',
        'bisque'=>'FFE4C4',
        'black'=>'000000',
        'blanchedalmond '=>'FFEBCD',
        'blue'=>'0000FF',
        'blueviolet'=>'8A2BE2',
        'brown'=>'A52A2A',
        'burlywood'=>'DEB887',
        'cadetblue'=>'5F9EA0',
        'chartreuse'=>'7FFF00',
        'chocolate'=>'D2691E',
        'coral'=>'FF7F50',
        'cornflowerblue'=>'6495ED',
        'cornsilk'=>'FFF8DC',
        'crimson'=>'DC143C',
        'cyan'=>'00FFFF',
        'darkblue'=>'00008B',
        'darkcyan'=>'008B8B',
        'darkgoldenrod'=>'B8860B',
        'darkgray'=>'A9A9A9',
        'darkgreen'=>'006400',
        'darkgrey'=>'A9A9A9',
        'darkkhaki'=>'BDB76B',
        'darkmagenta'=>'8B008B',
        'darkolivegreen'=>'556B2F',
        'darkorange'=>'FF8C00',
        'darkorchid'=>'9932CC',
        'darkred'=>'8B0000',
        'darksalmon'=>'E9967A',
        'darkseagreen'=>'8FBC8F',
        'darkslateblue'=>'483D8B',
        'darkslategray'=>'2F4F4F',
        'darkslategrey'=>'2F4F4F',
        'darkturquoise'=>'00CED1',
        'darkviolet'=>'9400D3',
        'deeppink'=>'FF1493',
        'deepskyblue'=>'00BFFF',
        'dimgray'=>'696969',
        'dimgrey'=>'696969',
        'dodgerblue'=>'1E90FF',
        'firebrick'=>'B22222',
        'floralwhite'=>'FFFAF0',
        'forestgreen'=>'228B22',
        'fuchsia'=>'FF00FF',
        'gainsboro'=>'DCDCDC',
        'ghostwhite'=>'F8F8FF',
        'gold'=>'FFD700',
        'goldenrod'=>'DAA520',
        'gray'=>'808080',
        'green'=>'008000',
        'greenyellow'=>'ADFF2F',
        'grey'=>'808080',
        'honeydew'=>'F0FFF0',
        'hotpink'=>'FF69B4',
        'indianred'=>'CD5C5C',
        'indigo'=>'4B0082',
        'ivory'=>'FFFFF0',
        'khaki'=>'F0E68C',
        'lavender'=>'E6E6FA',
        'lavenderblush'=>'FFF0F5',
        'lawngreen'=>'7CFC00',
        'lemonchiffon'=>'FFFACD',
        'lightblue'=>'ADD8E6',
        'lightcoral'=>'F08080',
        'lightcyan'=>'E0FFFF',
        'lightgoldenrodyellow'=>'FAFAD2',
        'lightgray'=>'D3D3D3',
        'lightgreen'=>'90EE90',
        'lightgrey'=>'D3D3D3',
        'lightpink'=>'FFB6C1',
        'lightsalmon'=>'FFA07A',
        'lightseagreen'=>'20B2AA',
        'lightskyblue'=>'87CEFA',
        'lightslategray'=>'778899',
        'lightslategrey'=>'778899',
        'lightsteelblue'=>'B0C4DE',
        'lightyellow'=>'FFFFE0',
        'lime'=>'00FF00',
        'limegreen'=>'32CD32',
        'linen'=>'FAF0E6',
        'magenta'=>'FF00FF',
        'maroon'=>'800000',
        'mediumaquamarine'=>'66CDAA',
        'mediumblue'=>'0000CD',
        'mediumorchid'=>'BA55D3',
        'mediumpurple'=>'9370D0',
        'mediumseagreen'=>'3CB371',
        'mediumslateblue'=>'7B68EE',
        'mediumspringgreen'=>'00FA9A',
        'mediumturquoise'=>'48D1CC',
        'mediumvioletred'=>'C71585',
        'midnightblue'=>'191970',
        'mintcream'=>'F5FFFA',
        'mistyrose'=>'FFE4E1',
        'moccasin'=>'FFE4B5',
        'navajowhite'=>'FFDEAD',
        'navy'=>'000080',
        'oldlace'=>'FDF5E6',
        'olive'=>'808000',
        'olivedrab'=>'6B8E23',
        'orange'=>'FFA500',
        'orangered'=>'FF4500',
        'orchid'=>'DA70D6',
        'palegoldenrod'=>'EEE8AA',
        'palegreen'=>'98FB98',
        'paleturquoise'=>'AFEEEE',
        'palevioletred'=>'DB7093',
        'papayawhip'=>'FFEFD5',
        'peachpuff'=>'FFDAB9',
        'peru'=>'CD853F',
        'pink'=>'FFC0CB',
        'plum'=>'DDA0DD',
        'powderblue'=>'B0E0E6',
        'purple'=>'800080',
        'red'=>'FF0000',
        'rosybrown'=>'BC8F8F',
        'royalblue'=>'4169E1',
        'saddlebrown'=>'8B4513',
        'salmon'=>'FA8072',
        'sandybrown'=>'F4A460',
        'seagreen'=>'2E8B57',
        'seashell'=>'FFF5EE',
        'sienna'=>'A0522D',
        'silver'=>'C0C0C0',
        'skyblue'=>'87CEEB',
        'slateblue'=>'6A5ACD',
        'slategray'=>'708090',
        'slategrey'=>'708090',
        'snow'=>'FFFAFA',
        'springgreen'=>'00FF7F',
        'steelblue'=>'4682B4',
        'tan'=>'D2B48C',
        'teal'=>'008080',
        'thistle'=>'D8BFD8',
        'tomato'=>'FF6347',
        'turquoise'=>'40E0D0',
        'violet'=>'EE82EE',
        'wheat'=>'F5DEB3',
        'white'=>'FFFFFF',
        'whitesmoke'=>'F5F5F5',
        'yellow'=>'FFFF00',
        'yellowgreen'=>'9ACD32');

$background_rule = strtolower($background_rule);
$background_rule = trim(str_replace('!important','',$background_rule));	
	
$rules = explode(' ',$background_rule);
	
foreach($rules as $key => $value){

if(array_key_exists($value,$colors)) return "match";

if(preg_match('/(rgb\(\s*\d{1,3},\s*\d{1,3},\s*\d{1,3}\)|\#[\w]{3,6})/i',$value)) return "match";	

}
	return "";
}
?>