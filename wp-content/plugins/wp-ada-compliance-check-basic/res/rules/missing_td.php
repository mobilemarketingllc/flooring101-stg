<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate tables missing data cells	
/********************************************************************/	
function wp_ada_compliance_basic_validate_missing_td($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('missing_td', $wp_ada_compliance_basic_scanoptions)) return 1;	

$tables = $dom->find('table');

foreach ($tables as $table) {	

$tablecode = $table->outertext;


if($table != "" and !stristr($tablecode, "<td") and $table->getAttribute("data-ninja_table_instance") == ""){	

	
	// save error
	if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"missing_td", $tablecode)){		
	$insertid = wp_ada_compliance_basic_insert_error($postinfo,"missing_td", $wp_ada_compliance_basic_def['missing_td']['StoredError'],  $tablecode);	
	}

	
}
}
return 1;
}

?>