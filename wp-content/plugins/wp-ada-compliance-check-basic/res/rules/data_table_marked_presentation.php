<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate data tables marked as presentation 	
/********************************************************************/	
function wp_ada_compliance_basic_validate_data_table_marked_presentation($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	$presentation_tables = get_option('wp_ada_compliance_basic_presentation_tables','true');	
	
// check if being scanned
if(in_array('data_table_marked_presentation', $wp_ada_compliance_basic_scanoptions)) return 1;	

$tables = $dom->find('table');

foreach ($tables as $table) {	

$tablecode = $table->outertext;		

if($table != "" 
   and (stristr($table->getAttribute('role'), "presentation") 
		or ($presentation_tables == 'true' and stristr($table->getAttribute('class'), "role-presentation"))) 
   and (stristr(wp_ada_compliance_basic_stripInnerTables($tablecode), "<th") 
		or $table->getAttribute('summary') != "" 
		or stristr(wp_ada_compliance_basic_stripInnerTables($tablecode), "<caption>"))
  ){
		
								// check for error from previous scan, such as custom fields
	
	// save error
	if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"data_table_marked_presentation", $tablecode)){		
	$insertid = wp_ada_compliance_basic_insert_error($postinfo,"data_table_marked_presentation", $wp_ada_compliance_basic_def['data_table_marked_presentation']['StoredError'],  $tablecode);	
	}
		
	}
	
}
return 1;
}

/*************************************************************
strip inner tables
*************************************************************/
function wp_ada_compliance_basic_stripInnerTables($html){
$dom = new \DOMDocument;
$dom->preserveWhiteSpace = false;
$dom->loadHTML($html);

// Declare array with numeric vlaues
$remainImages = array(0);

$nodes = $dom->getElementsByTagName("table");

  for($i = 0; $i < $nodes->length; $i++) {
    if (!in_array($i,$remainImages)) {
        $image = $nodes->item($i);
        $image->parentNode->removeChild($image);
     }  
}

$newhtml = $dom->saveHTML();
return $newhtml;	
}	
?>