<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
	
/********************************************************************/	
// validate lists with incorrect markup
/********************************************************************/	
function wp_ada_compliance_basic_validate_list_incorrect_markup($content, $postinfo){
		
global $wp_ada_compliance_basic_def;
	
// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('list_incorrect_markup', $wp_ada_compliance_basic_scanoptions)) return 1;
	
    $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");	
$dom = str_get_html($content);

	
$includes = array();
$excludes = array();
	
// all other bullets    
$includes['all'] = '/^(<br( ?\/){0,1}>)*\s*\({0,1}(∙|⁍|⁌|⁃|◦|‣|\p{Pd}|·|•|\(2\)|\(i\)|\(1\)|1\.|1\)|2-|a\.|a\))(\w|\s|[[:punct:]]|[^<(\/>)]){2,}$/um';	

//asterisks    
$includes['asterisks'] = '/^(<br( ?\/){0,1}>)*\s*\({0,1}(\*)(\w|\s|[[:punct:]]|[^<(\/>)]){2,}$/um';	
    
// exclude these    
$excludes[0] = '/^a\.(\w\.)/i';
$excludes[1] = '/^[0-9]\.[0-9]{1,2}/';
$excludes[2] = '/^\*\*/';
	
$elements = $dom->find('p,h1,h2,h3,h4,h5,h6');
	
foreach ($elements as $element) {
if(// other bullets
(preg_match($includes['all'],preg_replace("/&nbsp;/", "", trim(html_entity_decode ($element->plaintext)))) 
and !preg_match($excludes[0],trim($element->plaintext)) 
and !preg_match($excludes[1],trim($element->plaintext)) 
 )
// asterisks
   or (preg_match($includes['asterisks'],trim($element->plaintext))
   and !preg_match($excludes[2],trim($element->plaintext))
    and substr_count($element->plaintext, '*') > 1)
){
	$errorcode = $element->innertext;
			
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"list_incorrect_markup", $errorcode)){
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"list_incorrect_markup", $wp_ada_compliance_basic_def['list_incorrect_markup']['StoredError'],  $errorcode);
			}
			
			
}
}
	return 1;
} 

?>