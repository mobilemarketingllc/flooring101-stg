<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate tables missing th scope	
/********************************************************************/	
function wp_ada_compliance_basic_validate_missing_th_scope($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('missing_th_scope', $wp_ada_compliance_basic_scanoptions)) return 1;	

$tables = $dom->find('table');
	$k = 0;
foreach ($tables as $table) {	
    
// check for complext table ie... more than one header row or column
if(wp_ada_compliance_basic_check_for_complex_table($table)){    

$tablecode = $table->outertext;

$headercells = $table->find('th');

foreach ($headercells as $th) {
	if (isset($th) and ($th->getAttribute('scope') != "col" and $th->getAttribute('scope') != "row" and $th->getAttribute('scope') != "colgroup" and $th->getAttribute('scope') != "rowgroup" and $th->getAttribute('id') == "")) {
	
	
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"missing_th_scope", $tablecode))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"missing_th_scope",$wp_ada_compliance_basic_def['missing_th_scope']['StoredError'], $tablecode);	
			
			
		}

	}
}
}
return 1;
}

/************************************************************
check if table has multipel header rows or columns
************************************************************/
function wp_ada_compliance_basic_check_for_complex_table($table){
 
$headerrows = 0;   
$headercolumns = 0;
$tablerows = $table->find('tr');  
    
// test for table rows with a th cell
foreach ($tablerows as $row) {        
if(count($row->find('th')) > 0){
$headerrows++;   
}

// test for tables with more than one th in a single row
if(count($row->find('th')) > 1){
$headercolumns++;
}
}
if($headercolumns > 0 and $headerrows > 1) return 1;
    
return 0;
}
?>