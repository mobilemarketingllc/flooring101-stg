<?php 
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
//  check for anchors in the same page
/********************************************************************/	
function wp_ada_compliance_basic_validate_link_to_in_page_content($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('link_to_in_page_content', $wp_ada_compliance_basic_scanoptions)) return 1;	

$links = $dom->find('a');
foreach ($links as $link) {
 
$imagealtiscompliant = 0;
	// check embeded image for compliance
	$nodes = $link->children();
		foreach ($nodes as $node) {
        
			if($node->tag == "img" or $node->tag == "i") {
				if(stristr($node->getAttribute('alt'), __('in page link','wp-ada-compliance-basic'))
				   or stristr($node->getAttribute('title'), __('in page link','wp-ada-compliance-basic'))
				  or stristr($node->getAttribute('aria-label'),__('in page link','wp-ada-compliance-basic')) 
				  ){
				$imagealtiscompliant = 1;
				}
			}
           
			}
	if(substr($link->getAttribute('href'), 0, 1) == '#' 
       and strlen($link->getAttribute('href')) > 1
       and !stristr($link->plaintext, __('skip','wp-ada-compliance-basic')) 
        and !stristr($link->getAttribute('class'), __('skip-link','wp-ada-compliance-basic'))
       and !stristr($link->getAttribute('class'), __('screen-reader-text','wp-ada-compliance-basic')) 
        and !stristr($link->plaintext, __('in page link','wp-ada-compliance-basic')) 
       and !stristr($link->getAttribute('title'), __('in page link','wp-ada-compliance-basic')) 
       and !stristr($link->getAttribute('aria-label'), __('in page link','wp-ada-compliance-basic')) 
	   and $imagealtiscompliant == 0
      ) {
		    
	
			$code = $link->outertext;
		
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"link_to_in_page_content", $code))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"link_to_in_page_content", $wp_ada_compliance_basic_def['link_to_in_page_content']['StoredError'], $code);
			
		
	
}
}

	return 1;
}

?>