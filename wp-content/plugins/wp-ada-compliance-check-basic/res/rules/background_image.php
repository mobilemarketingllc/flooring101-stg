<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// look for background images inside css
/********************************************************************/	
function wp_ada_compliance_basic_validate_background_image($content, $postinfo){
global $wp_ada_compliance_basic_def;
		
// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('background_image', $wp_ada_compliance_basic_scanoptions)) return 1;

if($postinfo['type'] != 'css')	{
$dom = str_get_html($content);
	
foreach ($dom->find('*') as $element) {				
	
if(isset($element) 
and (stristr($element->getAttribute('style'), 'background:') 
   or stristr($element->getAttribute('style'), 'background-image:') )
   and stristr($element->getAttribute('style'),'url(')){ 
// save error
	$background_image_errorcode = wp_ada_compliance_basic_SimpleDOMRemoveChild($element);
	
		if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"background_image", $background_image_errorcode)){
			
		$insertid = wp_ada_compliance_basic_insert_error($postinfo, "background_image", $wp_ada_compliance_basic_def['background_image']['StoredError'], $background_image_errorcode);
		}
		

	
}
}
}
	
// // parse and scan style tag content in post files
if($postinfo['type'] != 'css' and stristr($content, '<style'))	{
// check links in content for style tags
$dom = str_get_html($content);

foreach ($dom->find('style') as $style) {	

	$css_array = wp_ada_compliance_basic_parce_style_content_for_background_url($style->innertext);
	
	wp_ada_compliance_basic_scan_css_content_for_background_url_violation($css_array, $postinfo);
}
}
// parse and scan css file content
if($postinfo['type'] == 'css')	{
$css_array = wp_ada_compliance_basic_parce_style_content_for_background_url($content);	

wp_ada_compliance_basic_scan_css_content_for_background_url_violation($css_array, $postinfo);

} 
}
	

/****************************************************************
parse css file to make it easy to search
*****************************************************************/
function wp_ada_compliance_basic_parce_style_content_for_background_url($css){
$css = preg_replace("%/\*(?:(?!\*/).)*\*/%s", " ",$css);	
$css_array = array(); // master array to hold all values
$element = explode('}', $css);
foreach ($element as $element) {
    // get the name of the CSS element
    $a_name = explode('{', $element);
    $name = $a_name[0];
    // get all the key:value pair styles
    $a_styles = explode(';', $element);
    // remove element name from first property element
    $a_styles[0] = str_replace($name . '{', '', $a_styles[0]);
    // loop through each style and split apart the key from the value
    $count = count($a_styles);
	$counter = 0;
    for ($a=0;$a<$count;$a++) {
        if ($a_styles[$a] != '') {
			$a_styles[$a] = str_ireplace('https://', '//', $a_styles[$a]);
			$a_styles[$a] = str_ireplace('http://', '//', $a_styles[$a]);
            $a_key_value = explode(':', $a_styles[$a]);
            // build the master css array
			if(array_key_exists(1, $a_key_value))
            $css_array[trim($counter.$name)][trim(strtolower($a_key_value[0]))] = trim($a_key_value[1]);
        }
		$counter++;
    }               
}
	return $css_array;
}
	
/*********************************************************
scan the content from a css file or style tag inside a post
*********************************************************/
function wp_ada_compliance_basic_scan_css_content_for_background_url_violation($css_array, $postinfo){
global $wp_ada_compliance_basic_def;	
foreach($css_array as $element => $rules){

if(array_key_exists('background', $rules) or array_key_exists('background-image', $rules))  {
	
if(array_key_exists('background-image', $rules)) $background = $rules['background-image'];
elseif(array_key_exists('background', $rules)) $background = $rules['background'];
else $background = "";
			
if(stristr($background,'url(')){
	

				$background_image_errorcode = $element.'{';
					foreach($rules as $key => $value){
						$background_image_errorcode .= $key.': '.$value.'; ';
					}
				$background_image_errorcode .= '}';

		// save error
		if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"background_image", $background_image_errorcode)){
			
		$insertid = wp_ada_compliance_basic_insert_error($postinfo, "background_image", $wp_ada_compliance_basic_def['background_image']['StoredError'], $background_image_errorcode);
		}
		

}
}

}
}
?>