<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate fieldsets without legends
/********************************************************************/	
function wp_ada_compliance_basic_validate_nested_fieldset($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('nested_fieldset', $wp_ada_compliance_basic_scanoptions)) return 1;	


	
$fieldsets = $dom->find('fieldset');
foreach ($fieldsets as $fieldset){
	
foreach ($fieldset->children() as $node){
	
    if ($node->tag == "fieldset"){
			
			$errorcode = $fieldset->outertext;
			
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"nested_fieldset", $errorcode))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"nested_fieldset", $wp_ada_compliance_basic_def['nested_fieldset']['StoredError'], $errorcode);
			

			
	}
}
}
	return 1;
}
?>