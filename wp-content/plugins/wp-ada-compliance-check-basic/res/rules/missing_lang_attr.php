<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate missing laguage attributes in theme files
/********************************************************************/
function wp_ada_compliance_basic_validate_missing_lang_attr($content, $postinfo){
	
global $wp_ada_compliance_basic_def;

// ignore check when scanning database only
if($postinfo['scantype'] == 'onsave') return 1;		
	
$dom = new DOMDocument;
libxml_use_internal_errors(true);
$dom->loadHTML($content);	
		
// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());

// check if being scanned
if(in_array('missing_lang_attr', $wp_ada_compliance_basic_scanoptions)) return 1;

// redundant title text
$html = $dom->getElementsByTagName('html');

foreach ($html as $htmlcode) {	
if ($htmlcode->getAttribute('lang') != "" or $htmlcode->getAttribute('xml:lang') != "") return 1;


$code = substr($dom->saveXML($htmlcode, LIBXML_NOEMPTYTAG), 0, strpos($dom->saveXML($htmlcode, LIBXML_NOEMPTYTAG), '>')+1);	

   
// save error
if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"missing_lang_attr", $code)){
$insertid = wp_ada_compliance_basic_insert_error($postinfo,"missing_lang_attr", $wp_ada_compliance_basic_def['missing_lang_attr']['StoredError'], $code);
}
}
}	
?>
