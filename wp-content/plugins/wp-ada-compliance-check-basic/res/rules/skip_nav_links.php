<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate skip nav links in theme files
/********************************************************************/
function wp_ada_compliance_basic_validate_skip_nav_links($content, $postinfo){

global $wp_ada_compliance_basic_def;

// ignore check when scanning database only
if($postinfo['scantype'] == 'onsave') return 1;	
	
$dom = new DOMDocument;
libxml_use_internal_errors(true);
$dom->loadHTML($content);	
		
// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());

// check if being scanned
if(in_array('skip_nav_links', $wp_ada_compliance_basic_scanoptions)) return 1;

$links = $dom->getElementsByTagName('a');
$count = 1;
foreach ($links as $link) {
	if (isset($link)){
	if($count==1 and !stristr($link->nodeValue, 'skip') or stristr($link->nodeValue, __('Skip to toolbar', 'wp-ada-compliance-basic'))) $notfirst=1;
     if (stristr($link->nodeValue, 'skip') and !stristr($link->nodeValue, __('Skip to toolbar', 'wp-ada-compliance-basic'))) $foundlink = 1;
	$count++;
}
}
if(isset($foundlink) and !isset($notfirst)) return 1;
	
if(isset($postinfo['externalsrc']))	{
	if(!isset($foundlink)) $code =  __('Missing skip links - iframe source', 'wp-ada-compliance-basic');	
	elseif(isset($notfirst)) $code =  __('Skip link not the first link in page - iframe source ', 'wp-ada-compliance-basic');

}
else{
if(!isset($foundlink)) $code =  __('Missing skip links', 'wp-ada-compliance-basic');	
elseif(isset($notfirst)) $code =  __('Skip link not the first link in page', 'wp-ada-compliance-basic');
}


// save error
if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"skip_nav_links", $code)){
$insertid = wp_ada_compliance_basic_insert_error($postinfo,"skip_nav_links", $wp_ada_compliance_basic_def['skip_nav_links']['StoredError'], $code);
}
			
}	
?>
