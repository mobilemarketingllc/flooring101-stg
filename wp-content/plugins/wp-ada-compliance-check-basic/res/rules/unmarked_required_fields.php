<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// form fields not marked as required
/********************************************************************/	
function wp_ada_compliance_basic_validate_unmarked_required_fields($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('unmarked_required_fields', $wp_ada_compliance_basic_scanoptions)) return 1;	


$forms = $dom->find('form');
foreach ($forms as $form){
 
	
		$errorcode = $form->outertext;
		if (!stristr($errorcode,__('required','wp-ada-compliance-basic')) and !stristr($errorcode, '*') and !stristr($errorcode, 'aria-required')){ 	
			
			if(wp_ada_compliance_basic_count_form_fields($errorcode) > 1){
			
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"unmarked_required_fields", $errorcode))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"unmarked_required_fields", $wp_ada_compliance_basic_def['unmarked_required_fields']['StoredError'], $errorcode);
			

			
			}
	}
}
	return 1;
}

// count number of fields
function wp_ada_compliance_basic_count_form_fields($errorcode) {
$count = 0;	
$minuscount = 0;	
$count += substr_count($errorcode, '<input');
$count += substr_count($errorcode, '<select');
$count += substr_count($errorcode, '<textarea');	
	
$minuscount += substr_count($errorcode, 'type="hidden"');
$minuscount += substr_count($errorcode, "type='hidden'");
$minuscount += substr_count($errorcode, "type='submit'");
$minuscount += substr_count($errorcode, 'type="submit"');		
$count = $count - $minuscount;
	
return $count;
}
?>