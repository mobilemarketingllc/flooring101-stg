<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
	
/********************************************************************/	
// validate incorrect use of line breaks to create whitespace
/********************************************************************/	
function wp_ada_compliance_basic_validate_incorrect_whitespace($content, $postinfo){
		
global $wp_ada_compliance_basic_def;
	
// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('incorrect_whitespace', $wp_ada_compliance_basic_scanoptions)) return 1;
	
$dom = str_get_html($content);
		
// tags to check
$tags = array('p','h1','h2','h3','h4','h5','h6','pre','code');	
	
foreach ($tags as $tag) {		
$elements = $dom->find($tag);
foreach ($elements as $element) {

//$patterns = array();
//$patterns[] = '\w(&nbsp;){6,}\w';	
$patterns = '(\s\w\s\w\s\w\s)|(\w( |\s|&nbsp;){6,}\w)';
//$patterns[] = '\w\s{6,}\w';
//$patterns[] = '\w( |\s|&nbsp;){6,}\w';
	
$html = $element->innertext;

//foreach ($patterns as $key => $value){	
if(preg_match('/' .$patterns.'/u',$html)){
	$errorcode = $element->outertext;

	
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"incorrect_whitespace", $errorcode)){
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"incorrect_whitespace", $wp_ada_compliance_basic_def['incorrect_whitespace']['StoredError'],  $errorcode);
			}
			
			
//}
}
}
}

	return 1;
} 

?>