<?php 
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate page structure, look for missing headings
/********************************************************************/	
function wp_ada_compliance_basic_validate_missing_section_headings($content, $postinfo){
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules', array());
    
// check if being scanned
if(in_array('missing_section_headings', $wp_ada_compliance_basic_scanoptions)) return 1;	    
  
/******************************************************************    
// deepscan heading check  look for section tags missing headings
*******************************************************************/
$elements = $dom->find('section');
	
foreach ($elements as $element) {
$h2 = count($element->find('h2'));
$h3 = count($element->find('h3'));
$h4 = count($element->find('h4'));
$h5 = count($element->find('h5'));
$h6 = count($element->find('h6'));
    
$headings = ($h2+$h3+$h4+$h5+$h6);    
    
if($headings == 0){
$errorcode = $element->outertext;
			
// save error
if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"missing_section_headings", $errorcode))
$insertid = wp_ada_compliance_basic_insert_error($postinfo,"missing_section_headings", $wp_ada_compliance_basic_def['missing_section_headings']['StoredError'], $errorcode);


}	    
    
}
    
return 1;
}
?>