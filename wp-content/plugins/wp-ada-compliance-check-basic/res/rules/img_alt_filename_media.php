<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// media library image alt text content contains file name
/********************************************************************/
function wp_ada_compliance_basic_validate_img_alt_filename_media($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('img_alt_filename_media', $wp_ada_compliance_basic_scanoptions)) return 1;	
	
$alt_text = get_post_meta($postinfo['postid'], '_wp_attachment_image_alt', true);
$imagecode = '<img src="'.esc_url(site_url()).'/wp-content/uploads/'.trim(strip_tags($content)).'" width="150" height="75" alt="'.$alt_text.'" />';	

        if (stristr($alt_text ,__('.jpg','wp-ada-compliance-basic')) 
			or stristr($alt_text ,__('.png','wp-ada-compliance-basic')) 
			or stristr($alt_text ,__('.gif','wp-ada-compliance-basic'))  
			or stristr($alt_text ,__('_','wp-ada-compliance-basic'))
						or preg_match('/^\d[a-zA-Z]\d[a-zA-Z][\w-_]+\d$/',$alt_text)
			or preg_match('/^[a-zA-Z]+\d$/',$alt_text)
		   ) {
			
			
			// check for error from previous scan, such as custom fields
		   	if(strstr($_SESSION['my_ada_notices'],$wp_ada_compliance_basic_def['img_alt_filename_media']['DisplayError'])) $foundInvalidalt = 1;
			
		 
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"img_alt_filename_media", $imagecode))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"img_alt_filename_media", $wp_ada_compliance_basic_def['img_alt_filename_media']['StoredError'], $imagecode);
			

			// display error
			if(!isset($foundInvalidalt) and !wp_ada_compliance_basic_ignore_check("img_alt_filename_media", $postinfo['postid'], $imagecode, $postinfo['type'])) {
				$_SESSION['my_ada_notices'] .= '<p>';
			$_SESSION['my_ada_notices'] .= $wp_ada_compliance_basic_def['img_alt_filename_media']['DisplayError'];
			if($wp_ada_compliance_basic_def['img_alt_filename_media']['Reference'] != "") $_SESSION['my_ada_notices'] .= ' <a href="'.$wp_ada_compliance_basic_def['img_alt_filename_media']['ReferenceURL'].'" target="_blank" class="adaNewWindowInfo">'.$wp_ada_compliance_basic_def['img_alt_filename_media']['Reference'].' <i class="fa fa-external-link" aria-hidden="true"><span class="wp_ada_hidden">'.__('opens in a new window', 'wp-ada-compliance-basic').'</span></i></a>';  
				$_SESSION['my_ada_notices'] .= $postinfo['ada_complianceview_error_link'];
			$_SESSION['my_ada_notices'] .= '</p>';
			
			$foundInvalidalt= 1;
			}
		
}
	return 1;
}
?>