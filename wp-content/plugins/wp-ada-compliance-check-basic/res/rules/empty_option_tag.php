<?php 
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************	
check for empty option tag	
********************************************************************/	
function wp_ada_compliance_basic_validate_empty_option_tag($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);	

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules', array());


// check if being scanned
if(in_array('empty_option_tag', $wp_ada_compliance_basic_scanoptions)) return 1;	

$elements = $dom->find('option');
foreach ($elements as $element) {

if (str_ireplace(array(' ','&nbsp;','-','_'),'',trim($element->plaintext)) == "" 
	and $element->getAttribute('aria-label') == "" 
	and $element->getAttribute('title') == ""
            and wp_ada_compliance_basic_get_aria_values($dom, $element, 'aria-labelledby') == ''
    and wp_ada_compliance_basic_get_aria_values($dom, $element, 'aria-describedby') == ''
  ) {
			
			$code = $element->outertext;
		
         
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"empty_option_tag", $code))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"empty_option_tag",$wp_ada_compliance_basic_def['empty_option_tag']['StoredError'], $code);	
            
		}
}
return 1;
}

?>