<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// image with alt text marked presentation
/********************************************************************/	
function wp_ada_compliance_basic_validate_img_alt_marked_presentation($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);	

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('img_alt_marked_presentation', $wp_ada_compliance_basic_scanoptions)) return 1;		
	
		
$images = $dom->find('img');	
	
foreach ($images as $image) {

if (isset($image) and $image->hasAttribute('alt') and $image->getAttribute('alt') != "" and $image->getAttribute('role') == "presentation") {
			
	
			$imagecode = $image->outertext;
			
			// ignore certain images
			if(wp_ada_compliance_basic_ignore_plugin_issues($imagecode))	goto img_alt_marked_presentationbottom;
			
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"img_alt_marked_presentation", $imagecode))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"img_alt_marked_presentation", $wp_ada_compliance_basic_def['img_alt_marked_presentation']['StoredError'], $imagecode);
			

			
		}
	img_alt_marked_presentationbottom:
		}
	

	return 1;
} 

?>