<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate nested tables	
/********************************************************************/	
function wp_ada_compliance_basic_validate_nested_tables($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
$presentation_tables = false;		
	
// check if being scanned
if(in_array('nested_tables', $wp_ada_compliance_basic_scanoptions)) return 1;	

$tables = $dom->find('table');

foreach ($tables as $table) {	

$tablecode = $table->outertext;
	
if($table != "" and count($table->find('table')) > 0){	
	
$tableinner = $table->find('table');	
	
foreach ($tableinner as $tablechild) {	

/*if(!stristr($tablechild->getAttribute('role'), "presentation") 
   and ($presentation_tables == 'false' 
		or ($presentation_tables == 'true' and !stristr($tablechild->getAttribute('class'), "role-presentation")))){*/

	
	// save error
	if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"nested_tables", $tablecode)){		
	$insertid = wp_ada_compliance_basic_insert_error($postinfo,"nested_tables", $wp_ada_compliance_basic_def['nested_tables']['StoredError'],  $tablecode);	
	}
		
//}
}
}

}
return 1;
}

?>