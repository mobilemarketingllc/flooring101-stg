<?php
// Exit if called directly.
if ( ! defined( 'ABSPATH' ) ) die;
/********************************************************************/	
// validate fieldsets without legends
/********************************************************************/	
function wp_ada_compliance_basic_validate_fieldset_without_legend($content, $postinfo){
	
global $wp_ada_compliance_basic_def;
	
$dom = str_get_html($content);		

// get options
$wp_ada_compliance_basic_scanoptions = get_option('wp_ada_compliance_basic_ignore_scan_rules',array());
	
// check if being scanned
if(in_array('fieldset_without_legend', $wp_ada_compliance_basic_scanoptions)) return 1;	


	$fieldsets = $dom->find('fieldset');

foreach ($fieldsets as $fieldset){
	
foreach ($fieldset->children() as $node){
if($node->tag == "legend" and preg_replace("/\s|&nbsp;/", '', htmlentities($node->plaintext, null, 'utf-8')) != "")	$legendfound = 1;
}
	
    if (!isset($legendfound)){
			
			$errorcode = $fieldset->outertext;
			
			// save error
			if(!$insertid = wp_ada_compliance_basic_error_check($postinfo,"fieldset_without_legend", $errorcode))
			$insertid = wp_ada_compliance_basic_insert_error($postinfo,"fieldset_without_legend", $wp_ada_compliance_basic_def['fieldset_without_legend']['StoredError'], $errorcode);
			

			
	}
}
	return 1;
}
?>